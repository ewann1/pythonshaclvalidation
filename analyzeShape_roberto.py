from pyshacl import validate


def make_validation(name):
    r = validate(name,
                 shacl_graph="https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/participant?v=development#",
                 inference='rdfs',
                 abort_on_first=False,
                 allow_infos=False,
                 allow_warnings=False,
                 meta_shacl=False,
                 advanced=True,
                 js=False,
                 debug=False)
    conforms, results_graph, results_text = r
    # print(results_text)
    return conforms


res = make_validation("person_roberto.json")
print("person_roberto.json should be conform >> " + str(res))
assert True == res
res = make_validation("person2.json")
print("person2.json should be conform >> " + str(res))
assert res == True
res = make_validation("person_invalid_roberto.json")
print("person_invalid_roberto should not be conform >> " + str(res))
assert res == False
res = make_validation("registration_vc.json")
print("registration_vc.json should be conform >> " + str(res))
assert res == True
res = make_validation("registration_vc_invalid.json")
print("registration_vc_invalid.json should not be conform >> " + str(res))
assert res == False
