from pyshacl import validate

def make_validation(name):
    r = validate(name,
                 shacl_graph="shapes.ttl",
                 inference='rdfs',
                 abort_on_first=False,
                 allow_infos=False,
                 allow_warnings=False,
                 meta_shacl=False,
                 advanced=True,
                 js=True,
                 debug=True)
    conforms, results_graph, results_text = r
    print(results_text)
    return conforms

res = make_validation("participant.json")
print("person.json should be conform >> " + str(res))
