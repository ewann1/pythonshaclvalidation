"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidateNewShapes = void 0;
const rdf_validate_shacl_1 = __importDefault(require("rdf-validate-shacl"));
const jsonld_1 = __importDefault(require("jsonld"));
const parser_n3_1 = __importDefault(require("@rdfjs/parser-n3"));
const stream_1 = require("stream");
const rdf_ext_1 = __importDefault(require("rdf-ext"));
const fs = __importStar(require("fs"));
class ValidateNewShapes {
    constructor() {
        this.shapePath = "../shapes.ttl";
        this.participantFilePath = "../participant.json";
    }
    validate(shapes, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const validator = new rdf_validate_shacl_1.default(shapes, { factory: rdf_ext_1.default });
            const report = yield validator.validate(data);
            const { conforms, results: reportResults } = report;
            console.log(conforms);
            console.log(reportResults.map((validationResult) => {
                return `${validationResult.path} /${validationResult.message}`;
            }));
        });
    }
    validateTest() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.jsonToRDFDataset(this.participantFilePath);
            const shapes = yield this.ttlToRDFDataset(this.shapePath);
            yield this.validate(shapes, data);
        });
    }
    jsonToRDFDataset(fileName) {
        return __awaiter(this, void 0, void 0, function* () {
            const jsonContent = JSON.parse(fs.readFileSync(fileName).toString());
            let quads;
            quads = yield (0, jsonld_1.default)().toRDF(jsonContent, { format: 'application/n-quads' });
            const parser = new parser_n3_1.default({ factory: rdf_ext_1.default });
            const stream = new stream_1.Readable();
            stream.push(quads);
            stream.push(null);
            return yield rdf_ext_1.default.dataset().import(parser.import(stream));
        });
    }
    ttlToRDFDataset(fileName) {
        return __awaiter(this, void 0, void 0, function* () {
            const rawContent = fs.readFileSync(fileName).toString();
            const parser = new parser_n3_1.default({ factory: rdf_ext_1.default });
            const stream = new stream_1.Readable();
            stream.push(rawContent);
            stream.push(null);
            return yield rdf_ext_1.default.dataset().import(parser.import(stream));
        });
    }
}
exports.ValidateNewShapes = ValidateNewShapes;
new ValidateNewShapes().validateTest();
//# sourceMappingURL=validate_new_shapes.js.map