import SHACLValidator from 'rdf-validate-shacl'
import jsonld from 'jsonld'
import DatasetExt from 'rdf-ext/lib/Dataset'
import Parser from '@rdfjs/parser-n3'
import {Readable} from 'stream'
import rdf from 'rdf-ext'
import * as fs from "fs";

export class ValidateNewShapes {
    shapePath = "../shapes.ttl"
    participantFilePath = "../participant.json"

    async validate(shapes: DatasetExt, data: DatasetExt) {
        const validator = new SHACLValidator(shapes, {factory: rdf as any})
        const report = await validator.validate(data)
        const {conforms, results: reportResults} = report
        console.log(conforms);
        console.log(reportResults.map((validationResult) => {
            return `${validationResult.path} /${validationResult.message}`
        }));
    }

    async validateTest() {

        const data = await this.jsonToRDFDataset(this.participantFilePath);
        const shapes = await this.ttlToRDFDataset(this.shapePath);
        await this.validate(shapes, data);

    }

    async jsonToRDFDataset(fileName: string): Promise<DatasetExt> {
        const jsonContent = JSON.parse(fs.readFileSync(fileName).toString())
        let quads;
        quads = await jsonld().toRDF(jsonContent, {format: 'application/n-quads'})

        const parser = new Parser({factory: rdf as any})

        const stream = new Readable()
        stream.push(quads)
        stream.push(null)

        return await rdf.dataset().import(parser.import(stream))
    }

    async ttlToRDFDataset(fileName: string): Promise<DatasetExt> {
        const rawContent = fs.readFileSync(fileName).toString();
        const parser = new Parser({factory: rdf as any})
        const stream = new Readable()
        stream.push(rawContent)
        stream.push(null)

        return await rdf.dataset().import(parser.import(stream))

    }

}

new ValidateNewShapes().validateTest()